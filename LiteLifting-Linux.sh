read file
VAR_0=$(cat $file| wc -c)
echo "This file has $VAR_0 characters in it."
VAR_1=$(cat $file|tr -d '\n'| wc -c)
echo "This file has $VAR_1 characters in it not counting newline."
VAR_2=$(grep -cvP '\S' $file)
echo "This file has $VAR_2 empty lines."
#head starts at beginning of file
#have to put tail so it only gives you the last line of the first 3
#cut -d' ' says use the space as a delimiter and so it separates the words by a space and then assigns the fourth word to the variable
#if number of lines is less than 3 
if [ $(cat $file|wc -l) -lt "3" ] ; then
        #if there is no third line, then print out a zero
        echo "The 4th word of the 3rd line of text is /0/."
#if number of words in line 3 is less than 4
elif [ $(head -3 $file|tail -1| wc -w) -lt "4" ] ; then
        #if there is a third line, but not a fourth word, then print an empty string
        echo "The 4th word of the 3rd line of text is //."
else
VAR_3=$(head -3 $file|tail -1|cut -d' ' -f4)
echo "The 4th word of the 3rd line of text is /$VAR_3/."
fi
if [[ $(sed ':a;N;$!ba;s/\n/ /g' $file) == *"Capital Idea"* ]]; then
  cat $file|tr [:upper:] [:lower:] > $file.lc
fi
