1. Elizabeth Bauch - Section B
I used Piazza, cplusplus.com, and the textbook to help me with this assignment to understand how stacks and queues work.

2. Challenges:
Some challenges I had was thinking about exactly how the "evil" part of hangman would work, but I have a lot of comments that represent my thought process. I thought about what would happen if I only had a few words I was handling, if I were playing hangman on a piece of paper, I was trying to keep my friend from winning, and changing the word in my head to keep them from winning. This helped me think through the logic of the project.

3. Likes/Dislikes:
I liked this assignment because I like the game hangman and it gave me some new ideas of how to make it more difficult for my opponents:)
The only thing I disliked, or wish was in the instructions was more examples of what should happen depending on what word length the user chooses. I like having something to compare to so I can for sure know it's working correctly.

4. I spent about 15 hours working on this assignment.