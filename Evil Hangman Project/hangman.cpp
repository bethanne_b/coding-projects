/*
    hangman.cpp
        
    Method implementations for the hangman class.
    
    assignment: CSCI 262 Project - Evil Hangman        

    author:

    last modified: 3/7/2019
*/

#include <vector>
#include <iostream>
#include "hangman.h"
#include <fstream>
#include <string>

using namespace std;

//family of the chosen word (all words of the same length)
//list of letters that have been chosen
//go through entire chosen family and for each word see if all of of the letters of that word have been guessed

//if fam = deal (that was the first word that matched with the LEAST letters guessed), and my guessedLetters are e and a
//then return -ea-
//if the word returned has no dashes in it, then the user won
//if num guessed reaches 0 and there are still dashes in the word, then the user lost

// constructor
//have to initialize variables in constructor because header file can't see syntax of assigning variables to anything
hangman::hangman() {
    max = 0;
    string word;
    guessedLetters = ""; //initializes the string that contains the letters guessed to an empty string
    int i = 0;
    ifstream words("dictionary.txt");
    if (!words) {
        cerr << "Error opening dictionary.txt" << endl;
    }
    else {
        while(!words.eof()) {
            words >> word;
            if (word.length() > max) {
                max = word.length();
            }
        }
        //cout << max << " is max" << endl;
        families = new vector <string> [max];
        ifstream words2("dictionary.txt");
        while (!words2.eof()) {
            words2 >> word;
            i = word.length();
            families[i - 1].push_back(word);
        }
    }
    //printing out the size of all the families test
//    for (int i = 0; i < max; i++) {
//        cout << families[i - 1].size() << endl;
//    }
}


// start_new_game()
//
// Setup a new game of hangman.
void hangman::start_new_game(int num_guesses, int word_length) {
    // TODO: Initialize game state variables

    fam = families[word_length - 1];
    lastWord = "";
    wordLength = word_length;
    numGuesses = num_guesses;
    guessedLetters = "";

}


// process_guess()
//

//keep track of letters guessed =DONE
//create logic that decides if game is won or not
//game is not won (lost) if they run out of guesses
//evil logic:
//do any of the words in fam match letters they've guessed?
//do any of the words in fam NOT match letters they've guessed?
//always choose a word that does not match guessed letters

// Process a player's guess - should return true/false depending on whether
// or not the guess was in the hidden word.  If the guess is incorrect, the
// remaining guess count is decreased.

//dog, cat, bed, sam
//guess a, now list has dog and bed
//guess o, now only bed
//guess e, and e has to be good because we
//if you can find any word in fam that has doesn't match the letters that are guessed

//they win if fam size reaches 1
bool hangman::process_guess(char c) {
    int i = 0;
    int j = 0;
    if (lastWord.size() > 0) {
        for (i = 0; i < lastWord.size(); i++) {
            if (lastWord.at(i) == c) {
                return true;
            }
        }
        return false;
    }
    //is that character in one of the words
    //then push onto matched letters list (vector)
    //check if the letter is in any words in fam
    for (i = 0; i < fam.size(); i++) {
        string word = fam.at(i);
        for (j = 0; j < word.size(); j++) {
            if (c == word.at(j)) {
                fam.erase(fam.begin() + i); //remove the word from the vector if the letter matches it
                i--;
                break;
            }
        }
        //if the letter is not in the word, because j reaches the end of the word without breaking
        if (fam.size() == 0) {
            lastWord = word;
        }
    }
    //if there are still words left that don't have any of the guessedLetters in them
    if (fam.size() > 0) {
        numGuesses--;
        return false;
    }
    return true;
}


// get_display_word()
//
// Return a representation of the hidden word, with unguessed letters
// masked by '-' characters.
//figure out if the letters guessed matches a word within the family
//if the letter guessed is in a word, then replace dashes with the letter that correspond to where the letter is in that word
//find the word with the lowest number of matched letters within that family
//once I find the least matched word then I can create that word with dashes in it
string hangman::get_display_word(int word_length) {
    int i = 0;
    int j = 0;
    string dashes = "";
    if (lastWord.size() == 0) {
        for (int i = 0; i < word_length; i++) {
            dashes += "-";
        }
    }
    else {
        for (i = 0; i < lastWord.size(); i++) {
            for (j = 0; j < guessedLetters.size(); j++) {
                if (lastWord.at(i) == guessedLetters.at(j)) {
                    dashes += lastWord.at(i);
                    break;
                }
            }
            if(j == guessedLetters.size()) {
                dashes += "-";
            }
        }
    }
    return dashes;
}

int hangman::famWords() {
    if (fam.size() == 0) {
        return 1; //to tell user there is one last word to guess, once they've guessed at least one letter in the word
    }
    return fam.size();
}

// get_guesses_remaining()
//
// Return the number of guesses remaining for the player.
int hangman::get_guesses_remaining(){
    return numGuesses;
}


// get_guessed_chars()
//
// What letters has the player already guessed?  Return in alphabetic order.
string hangman::get_guessed_chars() {
    return guessedLetters;
}


// was_char_guessed()
//
// Return true if letter was already guessed.
bool hangman::was_char_guessed(char c) {
    for (int i = 0; i < guessedLetters.size(); i++) {
        if (guessedLetters.at(i) == c) {
            return true;
        }
    }
    guessedLetters += c; //if the letter hasn't been guessed, then add the letter to the guessed letters string
    return false;
}


// is_won()
//
// Return true if the game has been won by the player.
bool hangman::is_won() {
    string word = get_display_word(wordLength);
    if (word == lastWord) {
        return true;
    }
    return false;
}


// is_lost()
//
// Return true if the game has been lost.
bool hangman::is_lost() {
    if (numGuesses == 0) {
        return true;
    }
    return false;
}


// get_hidden_word
//
// Return the true hidden word to show the player.
string hangman::get_hidden_word() {
    return lastWord;
}


