#ifndef _HANGMAN_H
#define _HANGMAN_H

/*
    hangman.h
        
    Class definition for the hangman class.

    assignment: CSCI 262 Project - Evil Hangman        

    author: 

    last modified: 9/24/2017
*/

#include <string>
#include <vector>

using namespace std;

/******************************************************************************
   class hangman

   Maintains game state for a game of hangman.

******************************************************************************/

class hangman {
public:
    hangman();

    int max; //max size of word
    int numGuesses; //number of guesses user chooses
    int wordLength; //length of word user chooses
    string guessedLetters; //letters that user guesses
    string lastWord; //last word that is left

    // start a new game where player gets num_guesses unsuccessful tries
	void start_new_game(int num_guesses, int word_length);

    // player guesses letter c; return whether or not char is in word
    bool process_guess(char c);

    // display current state of word - guessed characters or '-'
    string get_display_word(int);

    //displays the number of words in chosen family (chosen word size)
    int famWords();

    // How many guesses remain?
	int get_guesses_remaining();

    // What characters have already been guessed (for display)?
    string get_guessed_chars();

    // Has this character already been guessed?
    bool was_char_guessed(char c);

    // Has the game been won/lost?  (Else, it continues.)
    bool is_won();
    bool is_lost();

    // Return the true hidden word.
    string get_hidden_word();

    vector <string> *families;

    vector <string> fam;

    vector <char> matchedLetters;

private:
    
};

#endif
