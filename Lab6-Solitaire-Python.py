import random
def make_deck():
    deck = []
    suits = ['H', 'S', 'D', 'C']
    for i in range(len(suits)):
        ranks = ['A', '2' ,'3' ,'4', '5', '6', '7', '8', '9', 'X','J', 'Q', 'K']
        for j in range(len(ranks)):
            deck.append(suits[i]+ranks[j])
    return (deck)
deck = make_deck()

def deal_cards(deck):
    deck = make_deck()
    shuffled_deck = []
    n = 0
    for i in range(0,len(deck), 4):
        shuffled_deck.append(deck[i:i+4])
    return (shuffled_deck)
piles= deal_cards(deck)

def show_deck(piles, num):
    ranks = ['A', '2' ,'3' ,'4', '5', '6', '7', '8', '9', 'X','J', 'Q', 'K']
    print(end="    ")
    for i in range(len(ranks)):
        print(ranks[i], end=" ")
    print("")
    for row in range(4, 0, -1):
        print(row, end=": ")
        for shuffled_deck in range(13):
            if num == shuffled_deck and row == len(piles[shuffled_deck]):
                print(piles[shuffled_deck][-1], end=" ")
            elif row <= len(piles[shuffled_deck]):
                print("**", end=" ")
            else:
                print("  ", end=" ")
        print("")
        
import random
def main():
    create_deck = make_deck()
    random.shuffle(create_deck)
    deal_piles = deal_cards(create_deck)
    king_count = 0
    index = 12
    total_cards = 0
    ranks = ['A', '2' ,'3' ,'4', '5', '6', '7', '8', '9', 'X','J', 'Q', 'K']

    while king_count < 4:
        show_deck(piles, index)
        current_card = piles[index].pop(-1)
        if "K" in current_card:
            king_count = king_count + 1
        else:
            total_cards = total_cards + 1
        index = ranks.index(current_card[1])
        print("King count: ", king_count)
    if total_cards == 48:
        print("Winner winner, chicken dinner!")
    else:
        print("You took the L < NERD")
