/* Authors: Grace Clark and Elizabeth Bauch
 * C07A LevelGame
 */

package levelPieces;

import gameEngine.Drawable;
import gameEngine.InteractionResult;

public class Portal extends GamePiece {

	public Portal(char symbol, int location) {
		super(symbol, location);
	}

	@Override
	public InteractionResult interact(Drawable[] pieces, int playerLocation) {
		if ( this.getLocation() == playerLocation) {
			return InteractionResult.ADVANCE;
		}
		return InteractionResult.NONE;
	}
}
