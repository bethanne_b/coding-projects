/* Authors: Grace Clark and Elizabeth Bauch
 * C07A LevelGame
 */

package levelPieces;
import gameEngine.GameEngine;
import gameEngine.Drawable;
import gameEngine.Moveable;
import gameEngine.InteractionResult;
import java.util.Random;

public class Bug extends GamePiece implements Moveable {
	Random rand = new Random();
	
	public Bug(char symbol, int location) {
		super(symbol, location);
	}

	@Override
	public InteractionResult interact(Drawable[] pieces, int playerLocation) {
		if (this.getLocation() == playerLocation) {
			return InteractionResult.HIT;
		}
		return InteractionResult.NONE;
	}

	@Override
	public void move(Drawable[] gameBoard, int playerLocation) {
		int newLocation = rand.nextInt(GameEngine.BOARD_SIZE); //create new location within size of the board
		gameBoard[this.getLocation()] = null;
		this.setLocation(newLocation);
		gameBoard[this.getLocation()] = this;
	}

}
