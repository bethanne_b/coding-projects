/* Authors: Grace Clark and Elizabeth Bauch
 * C07A LevelGame
 */

package levelPieces;

import gameEngine.Drawable;
import gameEngine.InteractionResult;
import gameEngine.Moveable;

import java.lang.Math;

public class Dragon extends GamePiece implements Moveable {
	public final int FIRE_RANGE = 2;

	public Dragon(char symbol, int location) {
		super(symbol, location);
	}	

	@Override
	public InteractionResult interact(Drawable[] pieces, int playerLocation) {
		if ( Math.abs(this.getLocation() - playerLocation) == FIRE_RANGE ) {
			return InteractionResult.KILL;
		}
		return InteractionResult.NONE;
	}

	@Override
	public void move(Drawable[] gameBoard, int playerLocation) {
		int newLocation;
		if (this.getLocation() > playerLocation) {
			newLocation = this.getLocation() - 1;
		} else {
			newLocation = this.getLocation() + 1;
		}
		gameBoard[this.getLocation()] = null; //set to null so that there isn't more than one of this game piece at a time on the board
		this.setLocation(newLocation);
		gameBoard[this.getLocation()] = this;
		
	}
}
