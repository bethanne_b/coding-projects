/* Authors: Grace Clark and Elizabeth Bauch
 * C07A LevelGame
 */

package levelPieces;

import gameEngine.Drawable;

public class Setting implements Drawable {
	private String symbol;
	private int location;
	
	public Setting(String symbol, int location) {
		super();
		this.symbol = symbol;
		this.location = location;
	}
	
	public int getLocation() {
		return location;
	}

	@Override
	public void draw() {
		System.out.print(symbol);
	}
}
