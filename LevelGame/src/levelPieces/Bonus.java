/* Authors: Grace Clark and Elizabeth Bauch
 * C07A LevelGame
 */

package levelPieces;

import gameEngine.Drawable;
import gameEngine.InteractionResult;
import java.lang.Math;

public class Bonus extends GamePiece {
	public final int BONUS_DISTANCE = 2;
	
	public Bonus(char symbol, int location) {
		super(symbol, location);
	}

	@Override
	public InteractionResult interact(Drawable[] pieces, int playerLocation) {
		if ( Math.abs(this.getLocation() - playerLocation) < BONUS_DISTANCE) {
			return InteractionResult.GET_POINT;
		}
		return InteractionResult.NONE;
	}
}
