/* Authors: Grace Clark and Elizabeth Bauch
 * C07A LevelGame
 */

package levelPieces;

import java.util.Random;

import gameEngine.Drawable;
import gameEngine.InteractionResult;
import gameEngine.Moveable;

public class Ghost extends GamePiece implements Moveable {
	Random rand = new Random();

	public Ghost(char symbol, int location) {
		super(symbol, location);
	}

	@Override
	public InteractionResult interact(Drawable[] pieces, int playerLocation) {
		if ( playerLocation - this.getLocation() == 1) { //if player is directly in front of Ghost; movement will be constrained to only move forward
			int decision = rand.nextInt();
			if (decision % 2 == 0) {
				return InteractionResult.HIT;
			} else {
				return InteractionResult.GET_POINT;
			}
		}
		return InteractionResult.NONE;
	}

	@Override
	public void move(Drawable[] gameBoard, int playerLocation) { //debug so doesn't "erase" other game pieces?
		/*this.setLocation( this.getLocation() + 1);	
		gameBoard[this.getLocation()] = this;*/
		int newLocation = this.getLocation() + 1;
		gameBoard[this.getLocation()] = null; //set to null so that there isn't more than one of this game piece at a time on the board
		this.setLocation(newLocation);
		gameBoard[this.getLocation()] = this;
		
	}
}
