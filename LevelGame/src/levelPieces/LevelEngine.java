package levelPieces;
import java.util.ArrayList;
import gameEngine.Moveable;
import gameEngine.Drawable;
import gameEngine.GameEngine;

public class LevelEngine {
	private int levelNum;
	private Drawable [] board;
	private ArrayList<Moveable> movingPieces;
	private ArrayList <GamePiece> interactingPieces;
	private int playerStartLoc;
	
	public void createLevel(int levelNum) {
		switch (levelNum) {
		case 1:
			level1();
			break;
		case 2:
			level2();
			break;
		default:
			System.out.println("Level " + levelNum +  " not created yet.");
			break;	
		}
	}
	
	public void level1() {
		//create gamePieces
		Setting mountain = new Setting("M", 5);
		Ghost ghost = new Ghost('G', 2);
		Portal portal = new Portal('O', 8);
		Bonus bonus = new Bonus('B', 15);
		
		//set board
		Drawable [] board = new Drawable[GameEngine.BOARD_SIZE];
		board[mountain.getLocation()] = mountain;
		board[portal.getLocation()] = portal;
		board[ghost.getLocation()] = ghost;
		board[bonus.getLocation()] = bonus;
		this.board = board;
		
		//add any moving pieces to movingPieces
		ArrayList<Moveable> movingPieces = new ArrayList<Moveable>();
		movingPieces.add(ghost);
		this.movingPieces = movingPieces;
		
		//add any interacting pieces to interactingPieces;
		ArrayList <GamePiece> interactingPieces = new ArrayList<GamePiece>();
		interactingPieces.add(ghost);
		interactingPieces.add(portal);
		interactingPieces.add(bonus);
		this.interactingPieces = interactingPieces;
				
		//set playerStartLoc;
		this.playerStartLoc = 0;
	}
	
	public void level2() {
		//create gamePieces
		Setting mountain = new Setting("M", 5);
		Dragon dragon = new Dragon('D', 20);
		Portal portal = new Portal('O', 8);
		Bonus bonus = new Bonus('B', 15);
		
		//set board
		Drawable [] board = new Drawable[GameEngine.BOARD_SIZE];
		board[mountain.getLocation()] = mountain;
		board[portal.getLocation()] = portal;
		board[dragon.getLocation()] = dragon;
		board[bonus.getLocation()] = bonus;
		this.board = board;
		
		//add any moving pieces to movingPieces
		ArrayList<Moveable> movingPieces = new ArrayList<Moveable>();
		movingPieces.add(dragon);
		this.movingPieces = movingPieces;
		
		//add any interacting pieces to interactingPieces;
		ArrayList <GamePiece> interactingPieces = new ArrayList<GamePiece>();
		interactingPieces.add(dragon);
		interactingPieces.add(portal);
		interactingPieces.add(bonus);
		this.interactingPieces = interactingPieces;
		
		//set playerStartLoc;		
		this.playerStartLoc = 0;
	}
	
	public Drawable [] getBoard() {		
		return board;
	}
	
	public ArrayList<Moveable> getMovingPieces() {		
		return movingPieces;
	}
	
	public ArrayList<GamePiece> getInteractingPieces() {		
		return interactingPieces;
	}
	
	public int getPlayerStartLoc() {		
		return playerStartLoc;
	}
	
	
	
	
	
	
	
	
	
	
	
}
