//Authors: Elizabeth Bauch and Grace Clark

import static org.junit.Assert.*;

import org.junit.Test;

import java.lang.Math;

import gameEngine.Drawable;
import gameEngine.GameEngine;
import levelPieces.Bug;
import levelPieces.Dragon;
import levelPieces.Ghost;

public class TestMovingPieces {

	/* Strategy: 
	 * -place one random movement piece on board
	 * -run movement many times
	 * -ensure each space chosen at least once
	 */	
	@Test
	public void randomMovement() {
		Drawable [] gameBoard = new Drawable[GameEngine.BOARD_SIZE];
		Bug bug = new Bug('b',1);
		
		int [] spaceCounts = new int[GameEngine.BOARD_SIZE]; //create array to hold counts for each spot; initialized to zero
		
		for (int i = 0; i<10000; i++) {
			bug.move(gameBoard, 0);
			int location = bug.getLocation();
			spaceCounts[location]++; //if bug is in that spot, increment counter
		}
		
		for (int i = 0; i < GameEngine.BOARD_SIZE; i++) {
			assert(spaceCounts[i] > 0); //ensure counter for each space is greater than zero
		}	
	}
	
	/* Strategy:
	 * -place ghost on gameBoard
	 * -ensure that it moves exactly one space forward each turn before it falls off the board
	 */
	@Test
	public void forwardMovement() {
		Drawable [] gameBoard = new Drawable[GameEngine.BOARD_SIZE];
		Ghost ghost = new Ghost('g', 0);
		
		for (int i = 0; i < GameEngine.BOARD_SIZE - 1; i++) { //use one less than BOARD_SIZE, because on last loop Ghost will and should fall off the board
			int oldLocation = ghost.getLocation();
			ghost.move(gameBoard, 0);
			int newLocation = ghost.getLocation();
			assert(newLocation == oldLocation + 1); //ensure Ghost moved exactly one space forward
		}
	}
	
	
	/* Strategy (towardMovement 1 and 2):
	 * -place dragon on the board
	 * -test that dragon will move forward toward the player (if player in front of dragon);
	 * -test that dragon will move backwards toward the player (if player behind dragon);
	 * -i.e., test that absolute distance between dragon and player always decreases by one
	 */
	@Test
	public void towardMovement1() {
		//tests that piece moves toward player each time
		Drawable [] gameBoard = new Drawable[GameEngine.BOARD_SIZE];
		Dragon dragon = new Dragon('d', 0);
		//say player is at BOARD_SIZE
		for (int i = 0; i < GameEngine.BOARD_SIZE - 1; i++) {
			int OldDistance = Math.abs( GameEngine.BOARD_SIZE - dragon.getLocation());
			dragon.move(gameBoard, GameEngine.BOARD_SIZE - 1);
			int NewDistance = Math.abs( GameEngine.BOARD_SIZE - dragon.getLocation());
			assert(NewDistance < OldDistance );
		}	
	}
	
	@Test
	public void towardMovement2() {
		//tests that piece moves toward player each time
		Drawable [] gameBoard = new Drawable[GameEngine.BOARD_SIZE];
		Dragon dragon = new Dragon('d', GameEngine.BOARD_SIZE - 1);
		//say player is at 0
		for (int i = 0; i < GameEngine.BOARD_SIZE - 1; i++) {
			int OldDistance = Math.abs( 0 - dragon.getLocation());
			dragon.move(gameBoard, 0);
			int NewDistance = Math.abs( 0 - dragon.getLocation());
			assert(NewDistance < OldDistance );
		}
	}			
}
