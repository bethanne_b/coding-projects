//Authors: Elizabeth Bauch and Grace Clark
import static org.junit.Assert.*;

import gameEngine.InteractionResult;
import gameEngine.Drawable;
import gameEngine.GameEngine;
import levelPieces.Bug;
import levelPieces.Dragon;
import levelPieces.Ghost;
import levelPieces.Bonus;
import levelPieces.Portal;
import levelPieces.Setting;

import org.junit.Test;

public class TestInteractions {

	
	/* Strategy: 
	 * -make sure that if the Player and the Bug are on the same space that the interaction result is a HIT
	 * -otherwise, the interaction result should be NONE
	 */	
	@Test
	public void testBug() {
		int position = 0; //variable that can be used to change the location the Bug and Player are on when tested
		Drawable [] gameBoard = new Drawable[GameEngine.BOARD_SIZE];
		Bug bug = new Bug('b',position); 
		gameBoard[position] = bug; 
		// Hit points if Player on same space as bug
		assertEquals(InteractionResult.HIT, bug.interact(gameBoard, position));
		// This loop ensures no interaction if player and bug are not on same space
		for (int i=1; i<GameEngine.BOARD_SIZE; i++)	{
			assertEquals(InteractionResult.NONE, bug.interact(gameBoard, i));
		}
	}		
	
	/* Strategy: 
	 * -make sure that if the Player and the Bonus are next to each other, or on the same space, that the Player gets a point
	 * -otherwise, the interaction result should be NONE
	 */	
	@Test
	public void testBonus() {
		int position = 1; //variable that can be used to change the location the Bonus and Player are on when tested
		Drawable [] gameBoard = new Drawable[GameEngine.BOARD_SIZE];
		Bonus bonus = new Bonus('B',position); 
		gameBoard[position] = bonus; 
		//if Player is next to, or on the same space as Bonus, then they should get a bonus point
		assertEquals(InteractionResult.GET_POINT, bonus.interact(gameBoard, position));
		assertEquals(InteractionResult.GET_POINT, bonus.interact(gameBoard, position + 1));
		assertEquals(InteractionResult.GET_POINT, bonus.interact(gameBoard, position - 1));
		// Otherwise, this loop ensures no interaction if the player and Bonus are not next to each other or on the same space
		for (int i=3; i<GameEngine.BOARD_SIZE; i++)	{
			assertEquals(InteractionResult.NONE, bonus.interact(gameBoard, i));
		}
	}	
	
	/* Strategy: 
	 * -make sure that if the Player and the Dragon are within 2 spaces of each other, that the Player is killed
	 * -otherwise, the interaction result should be NONE
	 */	
	@Test
	public void testDragon() {
		int position = 2; //variable that can be used to change the location the Dragon and Player are on when tested
		Drawable [] gameBoard = new Drawable[GameEngine.BOARD_SIZE];
		Dragon dragon = new Dragon('d',position); 
		gameBoard[position] = dragon; 
		// Hit points if Player within 2 spaces from Dragon
		assertEquals(InteractionResult.KILL, dragon.interact(gameBoard, position + 2));
		assertEquals(InteractionResult.KILL, dragon.interact(gameBoard, position - 2));
		// This loop ensures no interaction if player and bug are not on same space
		for (int i=0; i<GameEngine.BOARD_SIZE; i++)	{
			//if the player is on space 0 or 4 (within two spaces of position 2), then they should be killed, otherwise, no interaction
			if (i != (position - 2) && i != (position + 2)) {
				assertEquals(InteractionResult.NONE, dragon.interact(gameBoard, i));
			}
		}
	}	

	/* Strategy: 
	 * -if the player is directly in front of the Ghost, then it randomly chooses to either have a HIT or GET_POINT interaction with the Player
	 * -however, this is somewhat difficult to test, because each time the function interact is called, a random decision is made
	 * -This means that we can only call interact once, because if we call it again the decision may change so the interaction may also change
	 * -Thus, we decided to assign the hit boolean to true if the interaction was a HIT, and if it was, then the if statement checks that
	 * it is true, and if it was not a HIT(a GET_POINT interaction), it checks that it is false
	 * -Then, we make sure that every other location produces the interaction result of NONE
	 */	
	@Test
	public void testGhost() {
		boolean hit = false; //boolean that is set to true if the Player is HIT or GET_POINT when directly in front of Ghost
		int position = 8; //variable that can be used to change the location the Ghost and Player are on when tested
		Drawable [] gameBoard = new Drawable[GameEngine.BOARD_SIZE];
		Ghost ghost = new Ghost('g',position); 
		gameBoard[position] = ghost; 
		// HIT or GET_POINT interaction if Player is directly in front of Ghost (depending on what is randomly chosen)
		hit = (InteractionResult.HIT == ghost.interact(gameBoard, position + 1));
		if (hit) {
			assertEquals(true, hit);
		}
		else {
			assertEquals(false, hit);
		}
		// This loop ensures no interaction if the Player is not directly in front of Ghost (location 4 in this case)
		for (int i=0; i<GameEngine.BOARD_SIZE; i++)	{
			if (i != (position + 1)) {
			assertEquals(InteractionResult.NONE, ghost.interact(gameBoard, i));
			}
		}
	}		
	
	/* Strategy: 
	 * -make sure that if the Player and the Portal are on the same space that the interaction result is an ADVANCE
	 * -otherwise, the interaction result should be NONE
	 */	
	@Test
	public void testPortal() {
		int position = 4; //variable that can be used to change the location the Portal and Player are on when tested
		Drawable [] gameBoard = new Drawable[GameEngine.BOARD_SIZE];
		Portal portal = new Portal('o',position); 
		gameBoard[position] = portal; 
		// ADVANCE if Player on same space as Portal
		assertEquals(InteractionResult.ADVANCE, portal.interact(gameBoard, position));
		// This loop ensures no interaction if Player and Portal are not on the same space
		for (int i=0; i<GameEngine.BOARD_SIZE; i++)	{
			if (i != position) {
				assertEquals(InteractionResult.NONE, portal.interact(gameBoard, i));
			}
		}		
	}

}	