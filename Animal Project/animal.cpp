//
// Created by BethAnne Bauch on 11/27/19.
//
/*
	animal.cpp

	author: L. Henke and C. Painter-Wakefield
	date: 04 November 2019

	Animal/20 questions program for CSCI 262, starter code.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <queue>

using namespace std;
static queue <char> answers;

class node {
public:
    string data;
    node* yes;
    node* no;
};

static node* root;
static string CR = "";
bool carriage = false;

void play_game(node*);
void process(ifstream &is, node *&dst);
node* read_game_tree();
bool isAnswer(node* curr);
void write_game_node(ofstream &is, node* curr);
void write_game_tree(node*);
void delete_game_tree(node*);
void game_history(node* curr);
void extend_tree (node* curr);


/**
 * Handles showing the main menu/basic UI
 */
int main() {
    root = read_game_tree();
    if (root == NULL) return -1;

    while (true) {
        string tmp;
        int choice;

        cout << "Welcome to 20 questions!" << endl;
        cout << "  1) Play the game" << endl;
        cout << "  2) Save the game file" << endl;
        cout << "  3) Quit" << endl;
        cout << "Please make your selection: ";
        getline(cin, tmp);
        choice = atoi(tmp.c_str()); //converts string to an integer
        //choice = 1;
        switch (choice) {
            case 1:
                play_game(root);
                break;
            case 2:
                write_game_tree(root);
                break;
            case 3:
                break;
            default:
                cout << "Sorry, I don't understand." << endl << endl;
        }
        if (choice == 3) break;
    }
    delete_game_tree(root);
    return 0;
}

/**
 * Sets up the recursive call to the read_preorder
 * @return root of the tree
 */

void process(ifstream &is, node *&dst) {
    string line;
    int pos = 3;
    dst = new node();
    getline(is, line);
    //if the file is in dos format it will have carriage returns
    if (strchr(line.c_str(),'\r') != NULL) {
      cout << "found CR" << endl;
      pos = 4;
      CR = "\r";
      carriage = true;
    }
    dst->data = line.substr(3, line.length()-pos);
    //cout << dst->data << endl;
    //cout << line << endl;
    //if string is answer
    if (line.at(1) == 'A') {
        return;
    }
    //recurse for yes
    process(is, dst->yes);
    //recurse for no
    process(is, dst->no);
}

node* read_game_tree() {
    node* r;
    ifstream file;
    file.open("animal_game_tree.txt");
    if (file.fail()) {
        cerr << "Could not open file" << endl;
        return NULL;
    }
    process(file, r);
    file.close();
    return r;
}

//bool function to determine if I have reached an answer node
bool isAnswer(node* curr) {
    return ((curr->yes == NULL) && curr->no == NULL);
}

/**
 * Plays the game
 * @param root Root of the game tree
 */
void play_game(node* curr) {
    //cout << "play game" << endl;
    char input;
    string input2 = "";
    cout << curr->data;
    cout << " (y/n): ";
    cin >> input;
    answers.push(input);
    if (isAnswer(curr)) {
        if (input == 'y') {
            cout << "YAY! I guessed your animal!" << endl;
        }
        else {
            cout << "BOO! I don't know!" << endl;
            cout << endl;
            cout << "Would you like to expand the game tree (y/n)? ";
            while (input2 == "") {
                getline(cin, input2);
            }
            if (input2 == "y") {
                cout << endl;
                cout << "I asked the following:" << endl;
                game_history(root);
                cout << endl;
                extend_tree(curr);
            }
        }
        return;
    }
    //if user chooses yes
    if (input == 'y') {
        //cout << "YES" << endl;
        play_game(curr->yes);
    }
    //if user chooses no
    if(input == 'n') {
        //cout << "NO" << endl;
        play_game(curr->no);
    }

}

void game_history(node* curr) {
    char front = answers.front();
    cout << curr->data;
    cout << ((front == 'y')?" YES":" NO") << endl;
    answers.pop();
    if (isAnswer(curr)) {
        return;
    }
    else {
        if (front == 'y') {
            game_history(curr->yes);
        }
        else {
            game_history(curr->no);
        }
    }
}

void extend_tree (node* curr) {
    string animal = "";
    string yesQ = "";
    string noQ = curr->data;
    cout << "Enter a new animal in the form of a question (e.g., 'Is it a whale?):" << endl;
    while (animal == "") {
        getline(cin, animal);
    }
    cout << "Now enter a question for which the answer is 'yes' for your new animal, and which does not contradict your previous answers:" << endl;
    while (yesQ == "") {
        getline(cin, yesQ);
    }
    cout << endl;
    curr->data = yesQ;
    curr->yes = new node();
    curr->yes->data = animal;
    curr->no = new node();
    curr->no->data = noQ;
}

void write_game_node(ofstream &is, node* curr) {
    //write the current node
    //if its an answers, you're done
    if (isAnswer(curr) && carriage == true) {
        is << "#A " << curr->data << CR << endl;
        return;
    }
    else if (isAnswer(curr)) {
        is << "#A " << curr->data << endl;
        return;
    }
    if (carriage == true) {
        is << "#Q " << curr->data << CR << endl;
    }
    else {
        is << "#Q " << curr->data << CR << endl;
    }
    //recurse yes
    write_game_node(is, curr->yes);
    //recurse no
    write_game_node(is, curr->no);
}

/**
 * Writes the game tree, sets up a recursive call to write_preorder();
 * @param root The root of the tree
 */
void write_game_tree(node* root) {
    cout << "Game file saved in 'animal_game_tree.txt'" << endl;
    cout << endl;
    //open stream
    ofstream file;
    file.open("animal_game_tree.txt");
    if (file.fail()) {
        cerr << "Could not open file to write to it" << endl;
        return;
    }
    //write root node
    write_game_node(file, root);
    //close the stream
    file.close();
}

void delete_game_node(node* curr) {
    if (isAnswer(curr)) {
        return;
    }
    delete_game_node(curr->yes);
    delete_game_node(curr->no);
    delete curr->yes;
    delete curr->no;
}
/**
 * Deletes the game tree
 * @param root Root of the game tree
 */
void delete_game_tree(node* root) {
    // Optional. Do a post-order deletion of the game tree.
    // This isn't strictly needed as the program exits after this is called,
    // which frees up all the memory anyway.
    delete_game_node(root);
    delete root;
}

