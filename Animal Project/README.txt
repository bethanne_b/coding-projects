1. Elizabeth Bauch - Section B
I used stack overflow, cplusplus.com, and the slides/textbook to understand how to use pointers and recursion for this project.

2. Some challenges I encountered was understanding exactly how to use recursion with the yes and leaves of the tree. Then I thought about it as if the recursive function calls were stairs and each call as a step, so that I could see that yes need to be recursed over first, and then the no had to be recursed over, to read over the game tree as it is presented in figure 1.
Another challenge I encountered was that the initial file was a dos file, which I learned with some google searches, I figured out
that has to do with Windows computers and carriage returns, so I put in some if statements to take care of that if the dos game tree file is used. That way it would rewrite the game file correctly, because before I did that it would miss some letters sometimes.

3. I liked this assignment, because I learned a lot about recursion and pointers which are difficult, but very useful concepts. I
disliked that the instructions for the output for the user were not consistent. For example, at the beginning of the assignment
instructions it said to do this:
"Enter a new animal in the form of a question,
e.g., 'Is it a whale?':"
but then at the end, it said to do it all in one line like so:
"Enter a new animal in the form of question (e.g. is it a platypus?):"
I would appreciate having some more consistent and clear instructions, because there were other things slightly different too, which makes me questions if I am doing something wrong, even though I am not.

4. I spent about 12 hours working on this assignment.

5. No extra credit implemented
